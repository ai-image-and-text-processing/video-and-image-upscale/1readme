# 1README

For upscaling videos I use https://video2x.org/
and then
https://github.com/hzwer/arXiv2020-RIFE

https://github.com/aka-katto/dandere2x (faster than Video2x but less good)
https://github.com/AaronFeng753/Waifu2x-Extension-GUI (easier GUI)

Alternatives:
https://github.com/QueerWorm/ds9-upscale/blob/master/guide.md
or
https://sites.google.com/view/wenbobao/dain


There are also these that are interesting
https://deoldify.ai/ to color movies that are in black and white

https://github.com/n00mkrad/cupscale to upscale pictures with a GUI
or https://github.com/Alexkral/AviSynthAiUpscale


Real time watch anime 4k upscaled: https://github.com/bloc97/Anime4K
